FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /junewayservice
COPY requirements.txt /junewayservice/ 
RUN pip install -r requirements.txt
COPY . /junewayservice/
